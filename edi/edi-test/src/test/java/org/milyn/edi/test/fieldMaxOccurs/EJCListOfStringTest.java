package org.milyn.edi.test.fieldMaxOccurs;

import org.junit.Test;
import org.milyn.edi.test.EJCTestUtil;
import org.milyn.edisax.EDIConfigurationException;
import org.milyn.edisax.util.IllegalNameException;
import org.xml.sax.SAXException;

import java.io.IOException;

public class EJCListOfStringTest {

    @Test
    public void definitionMap() throws EDIConfigurationException, IOException, SAXException, IllegalNameException {
        EJCTestUtil.testModel("odi-mapping.xml", "odi-input.txt", "DefinitionMapFactory", true);
    }

    @Test
    public void interchangeMessage() throws EDIConfigurationException, IOException, SAXException, IllegalNameException {
        EJCTestUtil.testModel("paores.xml", "odi-input.txt", "PaoresFactory", true);
    }
}
