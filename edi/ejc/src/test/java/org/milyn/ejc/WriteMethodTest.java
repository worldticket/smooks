package org.milyn.ejc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.milyn.edisax.model.internal.DelimiterType;
import org.milyn.edisax.model.internal.Field;
import org.milyn.edisax.model.internal.Segment;
import org.milyn.javabean.pojogen.JClass;
import org.milyn.javabean.pojogen.JNamedType;
import org.milyn.javabean.pojogen.JType;

import java.util.ArrayList;
import java.util.List;

public class WriteMethodTest {
    private static final String WRITE_METHOD_FIELD_OBJECT = "" +
            "\n" +
            "        Writer nodeWriter = new StringWriter();\n" +
            "\n" +
            "        List<String> nodeTokens = new ArrayList<String>();\n" +
            "\n" +
            "        if(c326 != null) {\n" +
            "            c326.write(nodeWriter, delimiters);\n" +
            "            nodeTokens.add(nodeWriter.toString());\n" +
            "            ((StringWriter)nodeWriter).getBuffer().setLength(0);\n" +
            "        }\n" +
            "        nodeTokens.add(nodeWriter.toString());\n" +
            "        writer.write(EDIUtils.concatAndTruncate(nodeTokens, DelimiterType.FIELD, delimiters));" +
            "";

    private static final String WRITE_METHOD_FIELD_COLLECTION = "" +
            "\n" +
            "        Writer nodeWriter = new StringWriter();\n" +
            "\n" +
            "        List<String> nodeTokens = new ArrayList<String>();\n" +
            "\n" +
            "        int c326Count = 0;\n" +
            "        if(c326 != null && !c326.isEmpty()) {\n" +
            "            for(C326 item : c326) {\n" +
            "                if(c326Count > 0) {\n" +
            "                    nodeWriter.write(delimiters.getField());\n" +
            "                }\n" +
            "                c326Count++;\n" +
            "                item.write(nodeWriter, delimiters);\n" +
            "                nodeTokens.add(nodeWriter.toString());\n" +
            "                ((StringWriter)nodeWriter).getBuffer().setLength(0);\n" +
            "            }\n" +
            "        } else {\n" +
            "            c326Count = 1;\n" +
            "        }\n" +
            "        for(int i = c326Count; i < 2; i++) {\n" +
            "            nodeTokens.add(delimiters.getField());\n" +
            "        }\n" +
            "        nodeTokens.add(nodeWriter.toString());\n" +
            "        writer.write(EDIUtils.concatAndTruncate(nodeTokens, DelimiterType.FIELD, delimiters));" +
            "";

    private static final String WRITE_METHOD_FIELD_OBJECT_AND_COLLECTION = "" +
            "\n" +
            "        Writer nodeWriter = new StringWriter();\n" +
            "\n" +
            "        List<String> nodeTokens = new ArrayList<String>();\n" +
            "\n" +
            "        if(c326 != null) {\n" +
            "            c326.write(nodeWriter, delimiters);\n" +
            "            nodeTokens.add(nodeWriter.toString());\n" +
            "            ((StringWriter)nodeWriter).getBuffer().setLength(0);\n" +
            "        }\n" +
            "        int c3262Count = 0;\n" +
            "        if(c3262 != null && !c3262.isEmpty()) {\n" +
            "            for(C326 item : c3262) {\n" +
            "                nodeWriter.write(delimiters.getField());\n" +
            "                c3262Count++;\n" +
            "                item.write(nodeWriter, delimiters);\n" +
            "                nodeTokens.add(nodeWriter.toString());\n" +
            "                ((StringWriter)nodeWriter).getBuffer().setLength(0);\n" +
            "            }\n" +
            "        }\n" +
            "        for(int i = c3262Count; i < 2; i++) {\n" +
            "            nodeTokens.add(delimiters.getField());\n" +
            "        }\n" +
            "        nodeTokens.add(nodeWriter.toString());\n" +
            "        writer.write(EDIUtils.concatAndTruncate(nodeTokens, DelimiterType.FIELD, delimiters));" +
            "";

    private static final String WRITE_VALUE_TRUNCATABLE_CODE = ""
            + "\n"
            + "        int placeLocationIdentificationCount = 0;\n"
            + "        if(placeLocationIdentification != null && !placeLocationIdentification.isEmpty()) {\n"
            + "            for(String item : placeLocationIdentification) {\n"
            + "                if(placeLocationIdentificationCount > 0) {\n"
            + "                    nodeWriter.write(delimiters.getField());\n"
            + "                }\n"
            + "                placeLocationIdentificationCount++;\n"
            + "                nodeWriter.write(delimiters.escape(item.toString()));\n"
            + "                nodeTokens.add(nodeWriter.toString());\n"
            + "                ((StringWriter)nodeWriter).getBuffer().setLength(0);\n"
            + "            }\n"
            + "        }\n"
            + "        for (int i = placeLocationIdentificationCount; i < 2; i++) {\n"
            + "            nodeTokens.add(delimiters.getField());\n"
            + "        }";

    private static final String WRITE_VALUE_NON_TRUNCATABLE_CODE = ""
            + "\n"
            + "        int placeLocationIdentificationCount = 0;\n"
            + "        if(placeLocationIdentification != null && !placeLocationIdentification.isEmpty()) {\n"
            + "            for(String item : placeLocationIdentification) {\n"
            + "                if(placeLocationIdentificationCount > 0) {\n"
            + "                    nodeWriter.write(delimiters.getField());\n"
            + "                }\n"
            + "                placeLocationIdentificationCount++;\n"
            + "                nodeWriter.write(delimiters.escape(item.toString()));\n"
            + "            }\n"
            + "        }\n"
            + "        for (int i = placeLocationIdentificationCount; i < 2; i++) {\n"
            + "            nodeWriter.write(delimiters.getField());\n"
            + "        }"
            + "";

    @Test
    public void fieldWithoutMaxOccurs() {
        JClass jFti = new JClass("com.example.common", "Fti");
        JClass jC326 = new JClass("com.example.common.composite", "C326");

        JType jTypeC326 = new JType(jC326.getSkeletonClass());
        JNamedType jNamedC326 = new JNamedType(jTypeC326, "c326");

        Segment fti = new Segment();
        fti.setSegcode("FTI");
        fti.setTruncatable(true);
        fti.setXmltag("FTI");

        Field c326 = new Field();
        c326.setTruncatable(true);
        c326.setXmltag("C326");
        c326.setNodeTypeRef("C326");
        c326.setRequired(true);
        fti.addField(c326);

        WriteMethod method = new WriteMethod(jFti, fti);
        method.writeObject(jNamedC326, DelimiterType.FIELD,null, c326);
        String result = method.getBody();
        assertEquals(WRITE_METHOD_FIELD_OBJECT, result);
    }

    @Test
    public void fieldWithMaxOccurs() {
        JClass jFti = new JClass("com.example.common", "Fti");
        JClass jC326 = new JClass("com.example.common.composite", "C326");

        JType jTypeC326 = new JType(jC326.getSkeletonClass());
        JType jTypeListOfC326 = new JType(List.class, jTypeC326.getType());
        JNamedType jNamedType = new JNamedType(jTypeListOfC326, "c326");

        Segment fti = new Segment();
        fti.setSegcode("FTI");
        fti.setTruncatable(true);
        fti.setXmltag("FTI");

        Field c326 = new Field();
        c326.setMaxOccurs(9);
        c326.setTruncatable(true);
        c326.setXmltag("C326");
        c326.setNodeTypeRef("C326");
        fti.addField(c326);

        WriteMethod method = new WriteMethod(jFti, fti);
        method.writeFieldCollection(jNamedType, DelimiterType.FIELD, 2);
        String result = method.getBody();
        assertEquals(WRITE_METHOD_FIELD_COLLECTION, result);
    }

    @Test
    public void fieldWithAndWithoutMaxOccurs() {
        JClass jFti = new JClass("com.example.common", "Fti");
        JClass jC326 = new JClass("com.example.common.composite", "C326");

        JType jTypeC326 = new JType(jC326.getSkeletonClass());
        JNamedType jNamedC326 = new JNamedType(jTypeC326, "c326");
        JType jTypeListOfC326 = new JType(List.class, jTypeC326.getType());
        JNamedType jNamedC3262 = new JNamedType(jTypeListOfC326, "c3262");

        Segment fti = new Segment();
        fti.setSegcode("FTI");
        fti.setTruncatable(true);
        fti.setXmltag("FTI");

        Field c326 = new Field();
        c326.setTruncatable(true);
        c326.setXmltag("C326");
        c326.setNodeTypeRef("C326");
        c326.setRequired(true);
        fti.addField(c326);

        Field c3262 = new Field();
        c3262.setMaxOccurs(8);
        c3262.setTruncatable(true);
        c3262.setXmltag("C326_2");
        c3262.setNodeTypeRef("C326");
        fti.addField(c3262);

        WriteMethod method = new WriteMethod(jFti, fti);
        method.writeObject(jNamedC326, DelimiterType.FIELD,null, c326);
        method.writeFieldCollection(jNamedC3262, DelimiterType.FIELD, 2);
        String result = method.getBody();
        assertEquals(WRITE_METHOD_FIELD_OBJECT_AND_COLLECTION, result);
    }

    @Test
    public void writeValue_simpleTypeWithMaxOccursAndTruncatable() {
        String result = newOdiSegmentAndWriteValue(true);
        assertEquals(WRITE_VALUE_TRUNCATABLE_CODE, result);
    }

    @Test
    public void writeValue_simpleTypeWithMaxOccursAndNotTruncatable() {
        String result = newOdiSegmentAndWriteValue(false);
        assertEquals(WRITE_VALUE_NON_TRUNCATABLE_CODE, result);
    }

    private String newOdiSegmentAndWriteValue(boolean truncatable) {
        JClass odiClass = new JClass("com.example.common", "Odi");

        JType listOfString = new JType(ArrayList.class, String.class);

        Segment odi = newOdiSegment(truncatable);

        Field field3225 = newField3225(truncatable);
        odi.addField(field3225);

        JNamedType location = new JNamedType(listOfString, "placeLocationIdentification");

        WriteMethod method = new WriteMethod(odiClass, odi);
        method.writeValue(location, field3225, DelimiterType.FIELD);
        return method.getBodyBuilder().toString();
    }

    private Field newField3225(boolean truncatable) {
        Field place = new Field();
        place.setTruncatable(truncatable);
        place.setXmltag("Place_location_identification");
        place.setDataType("String");
        place.setNodeTypeRef("3225");
        place.setMaxOccurs(2);
        return place;
    }

    private Segment newOdiSegment(boolean truncatable) {
        Segment odi = new Segment();
        odi.setSegcode("ODI");
        odi.setTruncatable(truncatable);
        odi.setXmltag("ODI");
        odi.setNodeTypeRef("ODI");
        return odi;
    }
}
